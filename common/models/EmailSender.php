<?php

namespace common\models;

use Yii;

/**
 * Class EmailSender
 * @package system\components
 * 
 * //common/main-local.php setting
 * 'mailer' => [
    'class' => 'yii\swiftmailer\Mailer',
    'viewPath' => '@common/mail',
    'transport' => [
        'class' => 'Swift_SmtpTransport',
        'host' => 'smtp.gmail.com',
        'username' => 'roket.kaktus.team@gmail.com',
        'password' => 'datsivkn46',
        'port' => '587',
        'encryption' => 'tls',
        'streamOptions' => [
            'ssl' => [
                'allow_self_signed' => true,
                'verify_peer' => false,
                'verify_peer_name' => false,
            ],
        ],
    ],
 */
class EmailSender
{

    /**
     * @param $address
     * @param $subject
     * @param $body
     */
    public static function sendEmail($addressTo, $subject, $html, $data = [])
    {
        return Yii::$app
            ->mailer
            ->compose(
                ['html' => $html],
                $data
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
            ->setTo($addressTo)
            ->setSubject($subject)
            ->send();
    }

}