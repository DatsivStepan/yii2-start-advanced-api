<?php
/**
 * @author DatsivStepan
 */

namespace common\components;

use Yii;


class AdminMenuUrlManager
{
    public function checkUrl($url)
    {
        return (\Yii::$app->request->pathInfo == $url)?'active':'';
    }
}