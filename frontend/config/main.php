<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'language'=>'en-EN',
        'i18n' => [
            'class'=> Zelenin\yii\modules\I18n\components\I18N::className(),
            'languages' => ['ru-RU', 'en-EN'],
            'translations' => [
                'yii' => [
                    'class' => yii\i18n\DbMessageSource::className()
                ],
                'eauth' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@eauth/messages',
                ],
            ]
        ],
        'eauth' => [
                'class' => 'nodge\eauth\EAuth',
                'popup' => true, // Use the popup window instead of redirecting.
                'cache' => false, // Cache component name or false to disable cache. Defaults to 'cache' on production environments.
                'cacheExpire' => 0, // Cache lifetime. Defaults to 0 - means unlimited.
                'httpClient' => [
                        // uncomment this to use streams in safe_mode
                        //'useStreamsFallback' => true,
                ],
                'services' => [ // You can change the providers and their classes.
                        'google' => [
                                // register your app here: https://code.google.com/apis/console/
                                'class' => 'nodge\eauth\services\GoogleOAuth2Service',
                                'clientId' => '...',
                                'clientSecret' => '...',
                                'title' => 'Google',
                        ],
                        'twitter' => [
                                // register your app here: https://dev.twitter.com/apps/new
                                'class' => 'nodge\eauth\services\TwitterOAuth1Service',
                                'key' => '...',
                                'secret' => '...',
                        ],
                        'yandex' => [
                                // register your app here: https://oauth.yandex.ru/client/my
                                'class' => 'nodge\eauth\services\YandexOAuth2Service',
                                'clientId' => '...',
                                'clientSecret' => '...',
                                'title' => 'Yandex',
                        ],
                        'facebook' => [
                                // register your app here: https://developers.facebook.com/apps/
                                'class' => 'nodge\eauth\services\FacebookOAuth2Service',
                                'clientId' => '115087659138274',
                                'clientSecret' => '0d8db075a04dba7d8586ee6e84a5950c',
                        ],
                        'yahoo' => [
                                'class' => 'nodge\eauth\services\YahooOpenIDService',
                                //'realm' => '*.example.org', // your domain, can be with wildcard to authenticate on subdomains.
                        ],
                        'linkedin' => [
                                // register your app here: https://www.linkedin.com/secure/developer
                                'class' => 'nodge\eauth\services\LinkedinOAuth1Service',
                                'key' => '...',
                                'secret' => '...',
                                'title' => 'LinkedIn (OAuth1)',
                        ],
                        'linkedin_oauth2' => [
                                // register your app here: https://www.linkedin.com/secure/developer
                                'class' => 'nodge\eauth\services\LinkedinOAuth2Service',
                                'clientId' => '...',
                                'clientSecret' => '...',
                                'title' => 'LinkedIn (OAuth2)',
                        ],
                        'github' => [
                                // register your app here: https://github.com/settings/applications
                                'class' => 'nodge\eauth\services\GitHubOAuth2Service',
                                'clientId' => '...',
                                'clientSecret' => '...',
                        ],
                        'live' => [
                                // register your app here: https://account.live.com/developers/applications/index
                                'class' => 'nodge\eauth\services\LiveOAuth2Service',
                                'clientId' => '...',
                                'clientSecret' => '...',
                        ],
                        'steam' => [
                                'class' => 'nodge\eauth\services\SteamOpenIDService',
                                //'realm' => '*.example.org', // your domain, can be with wildcard to authenticate on subdomains.
                                'apiKey' => '...', // Optional. You can get it here: https://steamcommunity.com/dev/apikey
                        ],
                        'instagram' => [
                                // register your app here: https://instagram.com/developer/register/
                                'class' => 'nodge\eauth\services\InstagramOAuth2Service',
                                'clientId' => '...',
                                'clientSecret' => '...',
                        ],
                        'vkontakte' => [
                                // register your app here: https://vk.com/editapp?act=create&site=1
                                'class' => 'nodge\eauth\services\VKontakteOAuth2Service',
                                'clientId' => '...',
                                'clientSecret' => '...',
                        ],
                        'mailru' => [
                                // register your app here: http://api.mail.ru/sites/my/add
                                'class' => 'nodge\eauth\services\MailruOAuth2Service',
                                'clientId' => '...',
                                'clientSecret' => '...',
                        ],
                        'odnoklassniki' => [
                                // register your app here: http://dev.odnoklassniki.ru/wiki/pages/viewpage.action?pageId=13992188
                                // ... or here: http://www.odnoklassniki.ru/dk?st.cmd=appsInfoMyDevList&st._aid=Apps_Info_MyDev
                                'class' => 'nodge\eauth\services\OdnoklassnikiOAuth2Service',
                                'clientId' => '...',
                                'clientSecret' => '...',
                                'clientPublic' => '...',
                                'title' => 'Odnoklas.',
                                ],
                ],
        ],
        'request' => [
            'baseUrl' => '',
            'csrfParam' => '_csrf-frontend',
            'class' => 'frontend\components\LangRequest',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                    'logFile' => '@app/runtime/logs/eauth.log',
                    'categories' => ['nodge\eauth\*'],
                    'logVars' => [],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'class'=>'frontend\components\LangUrlManager',
            'rules' => [
                '' => 'site/index',

                'users/<user_id:\d+>/posts' => 'user-posts/index',
                'users/<user_id:\d+>/posts/<id:\d+>' => 'user-posts/view',
                'users/<user_id:\d+>/posts/<id:\d+>/<_a:[\w-]+>' => 'user-posts/<_a>',
                'users/<user_id:\d+>/posts/<_a:[\w-]+>' => 'user-posts/<_a>',

                '<_c:[\w-]+>' => '<_c>/index',
                '<_c:[\w-]+>/<id:\d+>' => '<_c>/view',
                '<_c:[\w-]+>/<id:\d+>/<_a:[\w-]+>' => '<_c>/<_a>',
                'login/<service:google|facebook|etc>' => 'site/login',
            ],
        ],
    ],
    'params' => $params,
];
