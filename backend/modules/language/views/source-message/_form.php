<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\AdminGetParameter;
/* @var $this yii\web\View */
/* @var $model common\models\SourceMessage */
/* @var $form yii\widgets\ActiveForm */
$modelGetLang = new AdminGetParameter()
?>

<div class="source-message-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'category')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'message')->textInput(['maxlength' => true]) ?>
    <div class="well">
        <?php foreach($model->messagesModels as $lang => $translate){ ?>
            <?= $form->field($model, 'messagesModels['.$lang.']')->textInput(['maxlength' => true])->label('Message['.$lang.']') ?>        
        <?php } ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
